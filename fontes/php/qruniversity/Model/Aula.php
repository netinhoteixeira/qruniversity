<?php

require_once("QrModel.php");

Class Aula extends QrModel {

    var $id = null; //BIGINT
    var $semestre = null; //BIGINT
    var $sala = null; //BIGINT
    var $disciplina = null; //BIGINT
    var $professor = null; //BIGINT
    var $turma = null; //VARCHAR(5)
    var $diaSemana = null; //INT
    var $inicio = null; //TIME
    var $termino = null; //TIME
    //
    var $hash = null; //MD5(Classe.sigla)

    /**
     * @param null $diaSemana
     */

    public function setDiaSemana($diaSemana) {
        $this->diaSemana = $diaSemana;
    }

    /**
     * @return null
     */
    public function getDiaSemana() {
        return $this->diaSemana;
    }

    /**
     * @param null $disciplina
     */
    public function setDisciplina($disciplina) {
        $this->disciplina = $disciplina;
    }

    /**
     * @return null
     */
    public function getDisciplina() {
        return $this->disciplina;
    }

    /**
     * @param null $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param null $inicio
     */
    public function setInicio($inicio) {
        $this->inicio = $inicio;
    }

    /**
     * @return null
     */
    public function getInicio() {
        return $this->inicio;
    }

    /**
     * @param null $professor
     */
    public function setProfessor($professor) {
        $this->professor = $professor;
    }

    /**
     * @return null
     */
    public function getProfessor() {
        return $this->professor;
    }

    /**
     * @param null $sala
     */
    public function setSala($sala) {
        $this->sala = $sala;
    }

    /**
     * @return null
     */
    public function getSala() {
        return $this->sala;
    }

    /**
     * @param null $semestre
     */
    public function setSemestre($semestre) {
        $this->semestre = $semestre;
    }

    /**
     * @return null
     */
    public function getSemestre() {
        return $this->semestre;
    }

    /**
     * @param null $termino
     */
    public function setTermino($termino) {
        $this->termino = $termino;
    }

    /**
     * @return null
     */
    public function getTermino() {
        return $this->termino;
    }

    /**
     * @param null $turma
     */
    public function setTurma($turma) {
        $this->turma = $turma;
    }

    /**
     * @return null
     */
    public function getTurma() {
        return $this->turma;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash) {
        $this->hash = $hash;
    }

    /**
     * @return null
     */
    public function getHash() {
        return $this->hash;
    }

    function __construct() {
        parent::__construct();
    }

    function __destruct() {
        parent::__destruct();
    }

    function retrieve() {
        $query = '
        SELECT
          sl.nome as sala,
          a.turma,
          s.nome as semestre,
          ds.nome as diaSemana,
          a.inicio,
          a.termino,
          p.nome as professor,
          d.nome as disciplina,
          sl.hash
        FROM
          Sala sl
          LEFT JOIN Aula a ON
            (a.sala = sl.id)
          LEFT JOIN Semestre s ON
            (a.semestre = s.id)
          LEFT JOIN Disciplina d ON
            (a.disciplina = d.id)
          LEFT JOIN Professor p ON
            (a.professor = p.id)
          INNER JOIN DiaSemana ds ON
            (a.diaSemana = ds.id)
        ';
        if (isset($this->hash)) {
            $query .= 'WHERE sl.hash = "' . mysqli_escape_string($this->db, $this->hash) . '"';
        }

        $result = $this->db->query($query);

        //return $result->fetch_all(MYSQLI_ASSOC);
        // DONE: 2013-10-14 14:17 Fixed Fatal error: Call to undefined method mysqli_result::fetch_all()
        for ($res = array(); $tmp = $result->fetch_array(MYSQLI_ASSOC);)
            $res[] = $tmp;

        return $res;
    }

}
