package br.upis.tap.qruniversity.controller;

import java.util.ArrayList;
import java.util.List;

import br.upis.tap.qruniversity.R;
import br.upis.tap.qruniversity.adapter.AulaAdapter;
import br.upis.tap.qruniversity.entity.dto.Aula;
import br.upis.tap.qruniversity.service.ResultQRLoader;
import br.upis.tap.qruniversity.util.ITask;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ResultsActivity extends ListActivity implements ITask {

	private TextView txtSala;
	private Button btnReload;
	private String dataUrl;

	private SessionApplication session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);

		session = SessionApplication.getInstance();

		btnReload = (Button) findViewById(R.id.btn_result_reload);
		btnReload.setVisibility(View.INVISIBLE);

		txtSala = (TextView) findViewById(R.id.txt_result_sala);

		Intent it = getIntent();

		if (it != null) {
			dataUrl = it.getStringExtra("url");
		}

		if (session.getAulas() != null && !session.getAulas().isEmpty()) {
			btnReload.setVisibility(View.VISIBLE);
			btnReload.setOnClickListener(reload);

			mostrarResultado();
		}
	}

	private OnClickListener reload = new OnClickListener() {

		@Override
		public void onClick(View v) {
			reload();
		}
	};

	private void reload() {
		ResultQRLoader loader = new ResultQRLoader(this, ResultsActivity.this);
		loader.execute(dataUrl);
	}

	private void mostrarResultado() {
		List<Aula> aulas = session.getAulas();

		setListAdapter(new AulaAdapter(ResultsActivity.this,
				new ArrayList<Aula>()));

		if (aulas != null) {
			setListAdapter(new AulaAdapter(ResultsActivity.this, aulas));

			txtSala.setText(aulas.get(0).getSala());
		} else {
			txtSala.setText("Nenhum resultado encontrado");
		}

	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onCompleted(List<Aula> list) {
		session.setAulas(list);

		mostrarResultado();
	}

}