package br.upis.tap.qruniversity.camera;

import java.io.IOException;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {

	private SurfaceHolder mHolder;
	private Camera mCamera;
	private boolean preview = false;

	public CameraSurfaceView(Context context) {

		super(context);

		mHolder = getHolder();
		mHolder.addCallback(this);

		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

		if(preview && mCamera != null){			
			try{
				mCamera.setPreviewDisplay(mHolder);
			}catch(IOException e){
				Log.d(getClass().getName(),
						"Problemas ao parar a visualização da câmera: "
								+ e.getMessage());
			}
			
			start();
		}
	
	}
	
	public void start(){
		if(mCamera == null && !preview){
			mCamera = Camera.open();
			mCamera.setDisplayOrientation(90);
			
			mCamera.startPreview();
			preview = true;
		}
		
	}
	
	public void start(PreviewCallback previewCallback, AutoFocusCallback autoFocusCallback){		
		mCamera.setPreviewCallback(previewCallback);
		mCamera.autoFocus(autoFocusCallback);
		
		start();
	}
	
	public void stop(){
		preview = false;
		mCamera.setPreviewCallback(null);
		mCamera.stopPreview();
		 
		mCamera.release();
		mCamera = null;
	}
	
	public void setFocus(AutoFocusCallback autoFocusCallback){
		mCamera.autoFocus(autoFocusCallback);
	}
	
	public boolean isPreview(){
		return preview;
	}

}
