package br.upis.tap.qruniversity.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.upis.tap.qruniversity.entity.dto.Aula;
import br.upis.tap.qruniversity.util.ITask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class ResultQRLoader extends AsyncTask<String, Void, String> {

	private ITask task;
	private Context context;
	private ProgressDialog dialog;

	public ResultQRLoader(ITask task, Context ctx) {
		this.task = task;
		context = ctx;
	}

	@Override
	protected String doInBackground(String... params) {
		String result = "";

		try {
			for (String url : params) {
				result = getResult(url);
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(context);

		dialog.show();
	}

	@Override
	protected void onPostExecute(String result) {
		List<Aula> list = new ArrayList<Aula>();

		try {
			list = stringToList(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		dialog.dismiss();
		task.onCompleted(list);
	}

	@Override
	protected void onCancelled() {
		dialog.cancel();
	}

	private String getResult(String url) throws IOException, JSONException {
		HttpPost post = new HttpPost(url);
		HttpResponse resp = null;
		HttpEntity entity = null;
		DefaultHttpClient client = new DefaultHttpClient();

		resp = client.execute(post);
		entity = resp.getEntity();

		String result = EntityUtils.toString(entity);

		return result;
	}

	private List<Aula> stringToList(String json) throws JSONException {
		List<Aula> list = new ArrayList<Aula>();

		JSONObject obj = new JSONObject(json);

		JSONArray arr = obj.getJSONArray("registros");

		for (int i = 0; i < arr.length(); i++) {
			JSONObject row = arr.getJSONObject(i);

			Aula aula = new Aula(row.getString("sala"), row.getString("turma"),
					row.getString("inicio"), row.getString("termino"),
					row.getString("professor"), row.getString("disciplina"));

			list.add(aula);
		}

		return list;
	}

}
